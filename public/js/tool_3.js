
const tipo_moneda = document.querySelector('#tipo_moneda');
const cantidad_ahorrado = document.querySelector('#cantidad_ahorrado');
const tasa_inflacion = document.querySelector('#tasa_inflacion');
const number_years = document.querySelector('#number_years')


/*
* Eventos
* */
const btn_calculadora = document.querySelector('#btn_calcular');

const box_graph = document.querySelector('#box--graph');
const graph_text_result = document.querySelector('#graph_text_result');
const title_result_graph = document.querySelector('#title_result_graph');

const graph_section = document.querySelector('#graph_section');
const calculadora_section = document.querySelector('#calculadora--section');
/*
* valores html
* */
const texto_resultado = document.querySelector('#texto_resultado');
const valor_resultado = document.querySelector('#valor_resultado');

const percent_capital_ahorrado = document.querySelector('#first_percent');
const percent_capital_ahorrado_value = document.querySelector('#first_percent_value');

const percent_rentabilidad_alcanzada = document.querySelector('#second_percent');
const percent_rentabilidad_alcanzada_value = document.querySelector('#second_percent_value');

const validate = (input) => {
    const expNumber = /^[0-9]+$/;
    const value = expNumber.test(input);
    console.log(value);
    if(!value){
        error.classList.add('error-active');
        error.innerHTML = 'Ingrese cantidad válido';
        return value;
    }else{
        error.classList.remove('error-active');
        error.innerHTML = '';
        return value;
    }
}

const calculadora = ()=>{

    let moneda = tipo_moneda.value;
    let tasa = parseInt(tasa_inflacion.value);
    let anios = parseInt(number_years.value);
    let ahorro = parseInt(cantidad_ahorrado.value);


    if(ahorro !==0 && ahorro !== ''){
        if (validate(ahorro)){
            const impacto_inflacion = ahorro/(Math.pow((1+tasa/100), anios));
            return {
                'impacto':impacto_inflacion,
                'tasa':tasa,
                'moneda':moneda,
                'anio':anios,
                'ahorro':ahorro
            }
        }
    }
};

function numberConvert(n){
    n = n.toFixed(2).toString();
    while (true) {
        const n2 = n.replace(/(\d)(\d{3})($|,|\.)/g, '$1,$2$3')
        if (n === n2) break
        n = n2
    }
    return n;
}

const sendToHtml = ()=>{
    const data = calculadora();

    if (data){
        let {impacto, tasa, moneda, anio, ahorro} = data;

        calculadora_section.style.justifyContent = 'space-between';
        graph_section.style.display = 'block';

        box_graph.style.display = 'flex';
        graph_text_result.style.display = 'block';

        const valor_dinero = ( impacto*100)/ahorro;
        const valor_perdido = ((ahorro-impacto)*100)/ahorro;

        percent_capital_ahorrado_value.innerHTML = `${valor_dinero.toFixed(2)}%`;
        percent_rentabilidad_alcanzada_value.innerHTML = `${valor_perdido.toFixed(2)}%`;

        percent_capital_ahorrado.style.strokeDashoffset = `${489.5 - (489.5 * Math.round(valor_dinero)) / 100 }`;
        percent_rentabilidad_alcanzada.style.strokeDashoffset = `${313.8 - (313.8 * Math.round(valor_perdido)) / 100}`;

        texto_resultado.innerHTML = `Basado en una tasa de inflación del ${tasa}% anual, en ${anio} ${anio > 1 ? 'años':'año'}, el valor de su dinero en términos actuales podría ser:`;
        valor_resultado.innerHTML = `${moneda} ${numberConvert(impacto)}`;
    }
};

function eventListenners(){
    btn_calculadora.addEventListener('click', (e)=>{
        e.preventDefault();
        sendToHtml();
    });

    cantidad_ahorrado.addEventListener('input',()=>{
        validate(cantidad_ahorrado.value);
    })

}
eventListenners();