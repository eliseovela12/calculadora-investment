
document.getElementById('tasa_incremento').addEventListener('input', function(event){
    let value = event.target.value;
    document.getElementById('current-value-1').innerHTML = `${value}%`;
    document.getElementById('current-value-1').classList.add('active');
    document.getElementById('current-value-1').style.left = `${value*10}%`;
});

const costo_anual_actual = document.querySelector('#costo_anual_actual');
const back_year = document.querySelector('#back_year');
const next_year = document.querySelector('#next_year');
const incremento_anual = document.querySelector('#tasa_incremento')
const this_year = new Date();
/*
* Events
* */
const btn_calcular = document.querySelector('#btn_calcular');
const error = document.querySelector('#error');
const total_value_future = document.querySelector('#total_value_future');

/*
* Html values
* */
const list_result = document.querySelector('#list_result')
const total_now_value = document.querySelector('#current_value');

const graph_section = document.querySelector('#graph_section');
const calculadora_section = document.querySelector('#calculadora--section');

const validate = (input) => {
    const expNumber = /^[0-9]+$/;
    const value = expNumber.test(input);
    console.log(value);
    if(!value){
        error.classList.add('error-active');
        error.innerHTML = 'Ingrese cantidad válido';
        return value;
    }else{
        error.classList.remove('error-active');
        error.innerHTML = '';
        return value;
    }
}

const calculadora = ()=>{

    let anio_inicio = parseInt(back_year.value) + 2020;
    let anio_fin = parseInt(next_year.value) + 2020;
    let tasa = parseInt(incremento_anual.value)
    let costo_anual = parseInt(costo_anual_actual.value);
    let anio_actual = parseInt(this_year.getFullYear());

    if(costo_anual !== 0 && costo_anual !== '' && tasa !== 0){
        if (validate(costo_anual)) {
            /*
            * Calculamos desde el inicio del año en el que se esta realizando
            * */
            const diferencia_anios_hasta_inicio =  anio_inicio - anio_actual;
            let costo_escolar_hasta_inicio = [];

            for (let i = 0; i <= diferencia_anios_hasta_inicio ; i++) {
                costo_escolar_hasta_inicio.push( costo_anual + costo_anual*(Math.pow(1+tasa/100,i)-1));
            }

            /*
            *Continua desdee el año de inicio que selecciona el usuario,
            * es decir continua operando como si no hubiese seleccionado un año de inicio
            * */
            let costo_anual_normal = costo_escolar_hasta_inicio[diferencia_anios_hasta_inicio];

            const diferencia_anios = anio_fin - anio_inicio;
            let suma_total = 0;
            let costo_escolar = [];

            for (let i = 0; i <= diferencia_anios; i++) {
                costo_escolar.push(costo_anual_normal + costo_anual_normal*(Math.pow(1 + tasa/100, i)-1));
                suma_total += costo_escolar[i];
            }

            /*
            * Para simular el costo que le saldr�a en caso de querer aportar del mismo año que
            * realiza el calculo
            * */
            let suma_total_simulacion = 0;
            let costo_simulacion = [];

            for (let i = 0; i <= diferencia_anios; i++) {
                costo_simulacion.push(costo_anual + costo_anual*(Math.pow(1 + tasa/100, i)-1));
                suma_total_simulacion += costo_simulacion[i];
            }
            return {
                'costo_escolar':costo_escolar,
                'suma_total':suma_total,
                'anio': anio_inicio,
                'costo_anual':costo_anual,
                'suma_total_simulacion':suma_total_simulacion
            };

        }
    }
/*
    let moneda = tipo_moneda.value;
    let ahorro_total = parseInt(cantidad_ahorro_final.value);
    let tasa = parseInt(interes_anual.value);
    let anios = parseInt(tiempo_anual.value);

    if(ahorro_total !==0 && ahorro_total !== '' && tasa !== 0 && anios !== 0){
        if (validate(ahorro_total)){
            let number = anios*12;
            let interes_mensual = (tasa/100)/12;

            let ahorro_regular = (ahorro_total*interes_mensual)/(Math.pow(1+interes_mensual, number)-1);

            /!*                    console.log({
                                    'ahorro_regular':ahorro_regular,
                                    'moneda':moneda,
                                    'tasa':tasa,
                                    'anios':anios,
                                    'objetivo':ahorro_total
                                });*!/

            return {
                'ahorro_regular':ahorro_regular,
                'moneda':moneda,
                'tasa':tasa,
                'anios':anios,
                'objetivo':ahorro_total
            }
        }
    }*/
};

function numberConvert(n){
    n = n.toFixed(2).toString();
    while (true) {
        const n2 = n.replace(/(\d)(\d{3})($|,|\.)/g, '$1,$2$3')
        if (n === n2) break
        n = n2
    }
    return n;
}

const vaciarLista = ()=>{
    while (list_result.firstChild){
        list_result.removeChild(list_result.firstChild);
    }
}

const sendOnlyTotal = ()=>{
    const data = calculadora();

    if(data){
        let {suma_total_simulacion} = data;
        total_now_value.value = numberConvert(suma_total_simulacion);
    }
}

const sendToHtml = ()=>{

    vaciarLista();

    const data = calculadora();

    if(data){
        let {costo_escolar,anio,suma_total,suma_total_simulacion} = data;

        calculadora_section.style.justifyContent = 'space-between';
        graph_section.style.display = 'block';

        costo_escolar.forEach((data,key)=>{
            const row = document.createElement('div');
            row.classList.add('table--row');
            row.innerHTML = `
                <p class="years">Año ${anio+key}</p>
                <p class="cash">$${numberConvert(costo_escolar[key])}</p>
            `;
            list_result.appendChild(row);
        })

        total_value_future.innerHTML = `$${numberConvert(suma_total)}`;
        total_now_value.value = numberConvert(suma_total_simulacion);
    }
}

function eventListeners(){
    btn_calcular.addEventListener('click',(e)=>{
        e.preventDefault();
        sendToHtml()
    })

    costo_anual_actual.addEventListener('input',()=>{
        validate(costo_anual_actual.value);
    })

    back_year.addEventListener('change',()=>{
        sendOnlyTotal();
    })

    next_year.addEventListener('change',()=>{
        sendOnlyTotal();
    })

    incremento_anual.addEventListener('input',()=>{
        sendOnlyTotal();
    })

}
eventListeners();