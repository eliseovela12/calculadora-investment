
const tipo_moneda = document.querySelector('#tipo_moneda');
const cantidad_ahorro_final = document.querySelector('#cantidad_ahorro_final');
const tiempo_anual = document.querySelector('#tiempo_anual');
const interes_anual = document.querySelector('#interes_anual');

/*
* Eventtos
*/
const btn_calculadora = document.querySelector('#btn_calcular');
const error = document.querySelector('#error');

const box_graph = document.querySelector('#box--graph');
const graph_text_result = document.querySelector('#graph_text_result');
const title_result_graph = document.querySelector('#title_result_graph');
const graph_section = document.querySelector('#graph_section');
const calculadora_section = document.querySelector('#calculadora--section');
/*
* valores html
*/
const texto_resultado = document.querySelector('#texto_resultado');
const valor_resultado = document.querySelector('#valor_resultado');
const porcentaje_resultado = document.querySelector('#porcentaje_resultado');

const percent_capital_ahorrado = document.querySelector('#first_percent');
const percent_capital_ahorrado_value = document.querySelector('#first_percent_value');

const percent_rentabilidad_alcanzada = document.querySelector('#second_percent');
const percent_rentabilidad_alcanzada_value = document.querySelector('#second_percent_value');

const validate = (input) => {
    const expNumber = /^[0-9]+$/;
    const value = expNumber.test(input);
    console.log(value);
    if(!value){
        error.classList.add('error-active');
        error.innerHTML = 'Ingrese cantidad válido';
        return value;
    }else{
        error.classList.remove('error-active');
        error.innerHTML = '';
        return value;
    }
}

const calculadora = ()=>{

    let moneda = tipo_moneda.value;
    let ahorro_total = parseInt(cantidad_ahorro_final.value);
    let tasa = parseInt(interes_anual.value);
    let anios = parseInt(tiempo_anual.value);

    if(ahorro_total !==0 && ahorro_total !== '' && tasa !== 0 && anios !== 0){
        if (validate(ahorro_total)){
            let number = anios*12;
            let interes_mensual = (tasa/100)/12;

            let ahorro_regular = (ahorro_total*interes_mensual)/(Math.pow(1+interes_mensual, number)-1);

/*                    console.log({
                'ahorro_regular':ahorro_regular,
                'moneda':moneda,
                'tasa':tasa,
                'anios':anios,
                'objetivo':ahorro_total
            });*/

            return {
                'ahorro_regular':ahorro_regular,
                'moneda':moneda,
                'tasa':tasa,
                'anios':anios,
                'objetivo':ahorro_total
            }
        }
    }
};

function numberConvert(n){
    n = n.toFixed(2).toString();
    while (true) {
        const n2 = n.replace(/(\d)(\d{3})($|,|\.)/g, '$1,$2$3')
        if (n === n2) break
        n = n2
    }
    return n;
}

const sendToHtml = ()=>{
    const data = calculadora();

    if (data){
        let {ahorro_regular, moneda, tasa, anios, objetivo} = data;

        calculadora_section.style.justifyContent = 'space-between';
        graph_section.style.display = 'block';
        title_result_graph.style.display = 'block';
        box_graph.style.display = 'flex';
        graph_text_result.style.display = 'block';

        let capital_ahorrado = ((ahorro_regular*12*anios)*100)/objetivo;
        let rentabilidad_alcanzada = ((objetivo-(ahorro_regular*12*anios))*100)/objetivo;

        console.log('capital',capital_ahorrado);
        console.log('rentabilidad',rentabilidad_alcanzada);

        percent_capital_ahorrado_value.innerHTML = `${capital_ahorrado.toFixed(2)}%`;
        percent_rentabilidad_alcanzada_value.innerHTML = `${rentabilidad_alcanzada.toFixed(2)}%`;

        percent_capital_ahorrado.style.strokeDashoffset = `${489.5 - (489.5 * Math.round(capital_ahorrado)) / 100 }`;
        percent_rentabilidad_alcanzada.style.strokeDashoffset = `${313.8 - (313.8 * Math.round(rentabilidad_alcanzada)) / 100}`;

        texto_resultado.innerHTML = `Para alcanzar su objetivo de ${numberConvert(objetivo)} ${moneda} en ${anios} ${ anios > 1 ? `años`:`año`} , usted debe ahorrar:`;
        valor_resultado.innerHTML = `${moneda} ${numberConvert(ahorro_regular)}`;
        porcentaje_resultado.innerHTML = `Cada mes, basado en un crecimiento del ${tasa}% anual`;
    }
};

function eventListenners(){
    btn_calculadora.addEventListener('click', (e)=>{
            e.preventDefault();
        sendToHtml();
    });

    cantidad_ahorro_final.addEventListener('input',()=>{
        validate(cantidad_ahorro_final.value);
    })

}
eventListenners();
