
const tipo_moneda = document.querySelector('#tipo_moneda');
const edad_actual = document.querySelector('#edad_inicio');
const edad_jubilacion = document.querySelector('#edad_jubilacion');
const inflacion_anual = document.querySelector('#tasa_inflacion');

/*
* Datos personales
* */
const nombre = document.querySelector('#nombre');
const genero = document.querySelector('#genero');
/*
* Gastos mensuales
* */
const hipoteca = document.querySelector('#hipoteca');
const utilidades = document.querySelector('#utilidades');
const alimentos = document.querySelector('#alimentos');
const ropa = document.querySelector('#ropa');
const tv = document.querySelector('#tv');
const medicamentos = document.querySelector('#salud');
const transporte = document.querySelector('#transporte');
const otros = document.querySelector('#otros');

/*
* Events
* */
const btn_calcular = document.querySelector('#btn_calcular');
const error_edad_inicio = document.querySelector('#error_edad_inicio');
const error_edad_fin = document.querySelector('#error_edad_fin');
const error_hipoteca = document.querySelector('#error_hipoteca');
const error_utilidades = document.querySelector('#error_utilidades');
const error_alimentos = document.querySelector('#error_alimentos');
const error_ropa = document.querySelector('#error_ropa');
const error_tv = document.querySelector('#error_tv');
const error_salud = document.querySelector('#error_salud');
const error_transporte = document.querySelector('#error_transporte');
const error_otros = document.querySelector('#error_otros');

/*
* Html result
* */
const name_text = document.querySelector('#name_text');
const months_result = document.querySelector('#months_result');
const years_result = document.querySelector('#years_result');
const years_text = document.querySelector('#years_text');
const edad_promedio = document.querySelector('#edad_promedio_sexo');

const graph_section = document.querySelector('#graph_section');
const calculadora_section = document.querySelector('#calculadora--section');

const validateEdad = (input,edad) => {
    const expNumber = /^[0-9]{1,2}$|^75$/;
    const value = expNumber.test(input);
    console.log(value);
    if(edad){
        if(!value){
            error_edad_inicio.classList.add('error-active');
            error_edad_inicio.innerHTML = 'Ingrese edad válido';
            return value;
        }else{
            error_edad_inicio.classList.remove('error-active');
            error_edad_inicio.innerHTML = '';
            return value;
        }
    }else{
        if(!value){
            error_edad_fin.classList.add('error-active');
            error_edad_fin.innerHTML = 'Ingrese edad válido';
            return value;
        }else{
            error_edad_fin.classList.remove('error-active');
            error_edad_fin.innerHTML = '';
            return value;
        }
    }
}
const validateCash = (input,id) => {
    const expNumber = /^[0-9]+$/;
    const value = expNumber.test(input);
    console.log(value);
    if(!value){
        id.classList.add('error-active');
        id.innerHTML = 'Ingrese cantidad válido';
        return value;
    }else{
        id.classList.remove('error-active');
        id.innerHTML = '';
        return value;
    }

}

const calculadora = ()=>{

    let name = nombre.value;
    let sexo = parseInt(genero.value);

    let moneda = tipo_moneda.value;
    let edad_a = parseInt(edad_actual.value);
    let edad_f = parseInt(edad_jubilacion.value);
    let tasa = parseInt(inflacion_anual.value);

    let hipoteca_a = parseFloat(hipoteca.value);
    let utilidades_a = parseFloat(utilidades.value);
    let alimentos_a = parseFloat(alimentos.value);
    let ropa_a = parseFloat(ropa.value);
    let tv_a = parseFloat(tv.value);
    let salud_a = parseFloat(medicamentos.value);
    let transporte_a = parseFloat(transporte.value);
    let otros_a = parseFloat(otros.value);


    if(edad_a !==0 && edad_a !== '' && tasa !== 0 && edad_f !==0 && edad_f !== ''){
        if (validateCash(transporte.value, error_transporte) && validateCash(otros.value, error_otros)){
            const suma_total = hipoteca_a+utilidades_a+alimentos_a+ropa_a+tv_a+salud_a+transporte_a+otros_a;
            const diferencia_edad = edad_f-edad_a;
            const valor_anual_jubilacion = (suma_total*12)*(Math.pow((1+tasa/100),diferencia_edad));
            const valor_mensual_jubilacion = valor_anual_jubilacion/12;
            const valor_anual_actual = suma_total*12;

            return {
                'mensual_actual':suma_total,
                'anual_actual':valor_anual_actual,
                'anual_jubilacion':valor_anual_jubilacion,
                'mensual_jubilacion':valor_mensual_jubilacion,
                'moneda':moneda,
                'edad':diferencia_edad,
                'nombre':name,
                'genero':sexo,
                'edad_f':edad_f
            };
        }
    }

};

function numberConvert(n){
    n = n.toFixed(2).toString();
    while (true) {
        const n2 = n.replace(/(\d)(\d{3})($|,|\.)/g, '$1,$2$3')
        if (n === n2) break
        n = n2
    }
    return n;
}

const sendToHtml = ()=>{
    const data = calculadora();

    if (data){
        let {mensual_actual,anual_actual,anual_jubilacion,mensual_jubilacion,moneda,edad,nombre,genero,edad_f} = data;

        calculadora_section.style.justifyContent = 'space-between';
        graph_section.style.display = 'block';

            name_text.innerHTML = `${nombre}`;
            months_result.innerHTML = `${moneda}  ${numberConvert(mensual_actual)} <br> (${moneda} ${numberConvert(anual_actual)} por año)`;
            years_result.innerHTML = `${moneda}  ${numberConvert(mensual_jubilacion)} <br> (${moneda} ${numberConvert(anual_jubilacion)} por año)`;
            years_text.innerHTML = `Para cuando se jubile en ${edad} años, su gasto mensual podría haber aumentado a:`;

            console.log(typeof genero);

            if (genero){
                edad_promedio.innerHTML = `Hemos asumido que la esperanza de vida promedio de un hombre es de 78 años. Por lo tanto, según su edad de jubilación objetivo, es posible que deba financiar su estilo de vida durante la jubilación durante ${78 - edad_f} años.`;
            }else{
                edad_promedio.innerHTML = `Hemos asumido que la esperanza de vida promedio de una mujer es de 81 años. Por lo tanto, según su edad de jubilación objetivo, es posible que deba financiar su estilo de vida durante la jubilación durante ${81 - edad_f} años.`;
            }
    }
};

function eventListenners(){
    btn_calcular.addEventListener('click', (e)=>{
        e.preventDefault();
        sendToHtml();
    });

    edad_actual.addEventListener('input',()=>{
        validateEdad(edad_actual.value,true);
    });

    edad_jubilacion.addEventListener('input',()=>{
        validateEdad(edad_jubilacion.value,false);
    });

    hipoteca.addEventListener('input',()=>{
        validateCash(hipoteca.value,error_hipoteca);
    });

    utilidades.addEventListener('input',()=>{
        validateCash(utilidades.value, error_utilidades);
    });

    alimentos.addEventListener('input',()=>{
        validateCash(alimentos.value, error_alimentos);
    });

    ropa.addEventListener('input',()=>{
        validateCash(ropa.value, error_ropa);
    });

    tv.addEventListener('input',()=>{
        validateCash(tv.value, error_tv);
    });

    medicamentos.addEventListener('input',()=>{
        validateCash(medicamentos.value, error_salud);
    });

    transporte.addEventListener('input',()=>{
        validateCash(transporte.value, error_transporte);
    });

    otros.addEventListener('input',()=>{
        validateCash(otros.value, error_otros);
    });

}
eventListenners();