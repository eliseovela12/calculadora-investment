

/* 
* Barra de numeros porcentage
*/
document.getElementById('tasa_incremento').addEventListener('input', function(event){
    let value = event.target.value;
    document.getElementById('current-value-1').innerHTML = `${value}%`;
    document.getElementById('current-value-1').classList.add('active');
    document.getElementById('current-value-1').style.left = `${value*10}%`;
});

const tipo_moneda = document.querySelector('#moneda');
const inversion_anual = document.querySelector('#monto_invertir');
const tasa_anual = document.querySelector('#tasa_incremento');
const tiempo_inversion_anual = document.querySelector('#anios_inversion')
const inversion_mensual = document.querySelector('#ahorro_mensual');
/*
* evento
*/
const btn_calcular = document.querySelector('#btn_calcular')
const error_anual = document.querySelector('#error_anual');
const error_mensual = document.querySelector('#error_mensual');

const box_graph = document.querySelector('#box--graph');
const graph_text_result = document.querySelector('#graph_text_result');

const graph_section = document.querySelector('#graph_section');
const calculadora_section = document.querySelector('#calculadora--section');
/*
* valores html
*/
const patrimonio = document.querySelector('#patrimonio');
const rentabilidad = document.querySelector('#rentabilidad');

/*
* Valores para la grafica
*/
const percent_capital_ahorrado = document.querySelector('#first_percent');
const percent_capital_ahorrado_value = document.querySelector('#first_percent_value');

const percent_rentabilidad_alcanzada = document.querySelector('#second_percent');
const percent_rentabilidad_alcanzada_value = document.querySelector('#second_percent_value');


/*Extra event options */
//const change_options = document.querySelector('#change_options');

const validate = (input,type) => {
    const expNumber = /^[0-9]+$/;
    const value = expNumber.test(input);
    /* console.log(value); */
    if(type === 'anual'){
        if(!value){
            error_anual.classList.add('error-active');
            error_anual.innerHTML = 'Ingrese cantidad válido';
            return value;
        }else{
            error_anual.classList.remove('error-active');
            error_anual.innerHTML = '';
            return value;
        }
    }
    if(type === 'mensual'){
        if(!value){
            error_mensual.classList.add('error-active');
            error_mensual.innerHTML = 'Ingrese cantidad válido';
            return value;
        }else{
            error_mensual.classList.remove('error-active');
            error_mensual.innerHTML = '';
            return value;
        }
    }
}

const calculadora = ()=>{
    /*console.log(tipo_moneda.value);
    console.log(inversion_anual.value);
    console.log(tasa_anual.value);
    console.log(tiempo_inversion_anual.value);
    console.log(inversion_mensual.value);*/
    let moneda = tipo_moneda.value;
    let monto_anual = inversion_anual.value;
    let monto_mensual = inversion_mensual.value;
    let tasa = tasa_anual.value/100;
    let anios = tiempo_inversion_anual.value;


    if(monto_anual !== 0 && monto_anual !== ''){
        if(monto_mensual === ''){
            monto_mensual = 0;
        }
        if(validate(monto_mensual,'mensual') && validate(monto_anual,'anual')){
            const meses = 12;
            const total_periodos = meses*anios;
            const interes_mesual = tasa/meses;
            const z = 1 + interes_mesual;

            /*console.log({
                'meses':meses,
                'total periodos':total_periodos,
                'interes mensual':interes_mesual,
                'z' : z
            });*/

            const valor_final_mensual = monto_mensual*((Math.pow(z, total_periodos)-1)/interes_mesual);
            const valor_final_anual = monto_anual*((Math.pow(1 + tasa, anios)));
            const valor_final_total = valor_final_mensual + valor_final_anual;

            /*console.log({
                'valor final mensual':valor_final_mensual,
                'valor final anual': valor_final_anual,
                'valor final total':valor_final_total
            });*/

            const aporte_mensual_total = total_periodos*monto_mensual;
            const aporte_anual_total = monto_anual*1;
            const aporte_total = aporte_mensual_total+aporte_anual_total;

            /*console.log({
                'aporte mesual total':aporte_mensual_total,
                'aporte anual total':aporte_anual_total,
                'aporte total': aporte_total
            });*/

            const ganancia_total = valor_final_total - aporte_total;

            const porcentaje_rentabilidad = (100*ganancia_total)/valor_final_total;
            const porcentaje_capital = (100*aporte_total)/valor_final_total;

            return {
                'ganancia_total':ganancia_total,
                'valor_final_total':valor_final_total,
                'moneda':moneda,
                'rentabilidad_percent':porcentaje_rentabilidad,
                'capital_percent':porcentaje_capital
            }

        }
    }

};

function numberConvert(n){
    n = n.toFixed(2).toString();
    while (true) {
        const n2 = n.replace(/(\d)(\d{3})($|,|\.)/g, '$1,$2$3')
        if (n === n2) break
        n = n2
    }
    return n;
}

const vaciarLista = ()=>{
    while (change_options.firstChild){
        change_options.removeChild(change_options.firstChild);
    }
}

const sendToHtml = ()=>{
    //vaciarLista();

    const data = calculadora();

    if (data){
        let {ganancia_total, valor_final_total,moneda, rentabilidad_percent, capital_percent} = data;


        calculadora_section.style.justifyContent = 'space-between';
        graph_section.style.display = 'block';
        box_graph.style.display = 'flex';
        graph_text_result.style.display = 'block';

        patrimonio.innerHTML = `${moneda}${numberConvert(valor_final_total)}`;
        rentabilidad.innerHTML = `${moneda}${numberConvert(ganancia_total)}`;

        /* console.log(`Rentabilidad alcanzada ${rentabilidad_percent}`);
        console.log(`Capital ahorrado ${capital_percent}`); */

        percent_capital_ahorrado_value.innerHTML = `${capital_percent.toFixed(2)}%`;
        percent_rentabilidad_alcanzada_value.innerHTML = `${rentabilidad_percent.toFixed(2)}%`;

        percent_capital_ahorrado.style.strokeDashoffset = `${489.5 - (489.5 * Math.round(capital_percent)) / 100 }`;
        percent_rentabilidad_alcanzada.style.strokeDashoffset = `${313.8 - (313.8 * Math.round(rentabilidad_percent)) / 100}`;


    }
};

function eventListenners(){
    btn_calcular.addEventListener('click',(e)=>{
        e.preventDefault();
        sendToHtml();
    })
    inversion_anual.addEventListener('input',()=>{
        validate(inversion_anual.value,'anual');
    })
    inversion_mensual.addEventListener('input',()=>{
        validate(inversion_mensual.value,'mensual');

    })

}
eventListenners();
